var removeSVG = '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="774.266px" height="774.266px" viewBox="0 0 774.266 774.266" style="enable-background:new 0 0 774.266 774.266;" xml:space="preserve"><g><g><path d="M640.35,91.169H536.971V23.991C536.971,10.469,526.064,0,512.543,0c-1.312,0-2.187,0.438-2.614,0.875C509.491,0.438,508.616,0,508.179,0H265.212h-1.74h-1.75c-13.521,0-23.99,10.469-23.99,23.991v67.179H133.916c-29.667,0-52.783,23.116-52.783,52.783v38.387v47.981h45.803v491.6c0,29.668,22.679,52.346,52.346,52.346h415.703c29.667,0,52.782-22.678,52.782-52.346v-491.6h45.366v-47.981v-38.387C693.133,114.286,670.008,91.169,640.35,91.169zM285.713,47.981h202.84v43.188h-202.84V47.981zM599.349,721.922c0,3.061-1.312,4.363-4.364,4.363H179.282c-3.052,0-4.364-1.303-4.364-4.363V230.32h424.431V721.922z M644.715,182.339H129.551v-38.387c0-3.053,1.312-4.802,4.364-4.802H640.35c3.053,0,4.365,1.749,4.365,4.802V182.339z"/><rect x="475.031" y="286.593" width="48.418" height="396.942"/><rect x="363.361" y="286.593" width="48.418" height="396.942"/><rect x="251.69" y="286.593" width="48.418" height="396.942"/></g></g></svg><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="774.266px" height="774.266px" viewBox="0 0 774.266 774.266" style="enable-background:new 0 0 774.266 774.266;" xml:space="preserve"><g><g><path d="M640.35,91.169H536.971V23.991C536.971,10.469,526.064,0,512.543,0c-1.312,0-2.187,0.438-2.614,0.875C509.491,0.438,508.616,0,508.179,0H265.212h-1.74h-1.75c-13.521,0-23.99,10.469-23.99,23.991v67.179H133.916c-29.667,0-52.783,23.116-52.783,52.783v38.387v47.981h45.803v491.6c0,29.668,22.679,52.346,52.346,52.346h415.703c29.667,0,52.782-22.678,52.782-52.346v-491.6h45.366v-47.981v-38.387C693.133,114.286,670.008,91.169,640.35,91.169zM285.713,47.981h202.84v43.188h-202.84V47.981zM599.349,721.922c0,3.061-1.312,4.363-4.364,4.363H179.282c-3.052,0-4.364-1.303-4.364-4.363V230.32h424.431V721.922z M644.715,182.339H129.551v-38.387c0-3.053,1.312-4.802,4.364-4.802H640.35c3.053,0,4.365,1.749,4.365,4.802V182.339z"/><rect x="475.031" y="286.593" width="48.418" height="396.942"/><rect x="363.361" y="286.593" width="48.418" height="396.942"/><rect x="251.69" y="286.593" width="48.418" height="396.942"/></g></g></svg>';
var completeSVG =   '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7C514.5,101.703,514.499,85.494,504.502,75.496z"/></g></g></svg>';

var data = (localStorage.getItem('todoList'))?JSON.parse(localStorage.getItem('todoList')) : {
  todo : [],
  completed : []
};
renderTodoList();

document.getElementById('add').addEventListener('click',function(){
  var value = document.getElementById('item').value;
  if(!value){
    alert('لطفا یک فعالیت اضافه کنید!');
  }else{
    addItem(value);
  }
});

document.getElementById('item').addEventListener('keydown',function(e){
  var value = this.value;
  if(e.code === 'Enter' && value){
    addItem(value);
  }
});


function addItem(value){
  addItemTodo(value);
  document.getElementById('item').value = '';
  data.todo.push(value);
  dataObjectUpdate();
}

function renderTodoList(){
  if(!data.todo.length && !data.completed.length )return;

  for(var i=0; i < data.todo.length; i++){
    var value = data.todo[i];
    addItemTodo(value);

  }

  for(var j=0; j < data.completed.length; j++){
    var value = data.completed[j];
    addItemTodo(value,true);
  }
}

function dataObjectUpdate(){
  localStorage.setItem('todoList' , JSON.stringify(data) );
}

function removeItem(){
  var item = this.parentNode.parentNode
  var parent = item.parentNode;
  var parentID = parent.id;
  var value = item.innerText;

  if(parentID == 'todo'){
    data.todo.splice(data.todo.indexOf(value),1);
  }else{
    data.completed.splice(data.completed.indexOf(value),1);
  }
  dataObjectUpdate();

  parent.removeChild(item);

 }

function completeItem(){
  var item = this.parentNode.parentNode;
  var parent = item.parentNode;
  var parentID = parent.id;
  var value = item.innerText;

  if(parentID == 'todo'){
    data.todo.splice(data.todo.indexOf(value),1);
    data.completed.push(value);
  }else{
    data.completed.splice(data.completed.indexOf(value),1);
    data.todo.push(value);
  }
  dataObjectUpdate();
  

  var target = (parentID == 'todo')? document.getElementById('completed') : document.getElementById('todo');

  parent.removeChild(item);
  target.insertBefore(item,target.childNodes[0]);
 
}

function addItemTodo(text , completed) {
   var list = (completed) ? document.getElementById('completed') : document.getElementById('todo');


   var item = document.createElement('li');
   item.innerText = text;

   var button = document.createElement('div');
   button.classList.add('button');

   var remove = document.createElement('button');
   remove.classList.add('remove');
   remove.innerHTML = removeSVG;

  //Add click event for removing item
  remove.addEventListener('click',removeItem);


   var complete = document.createElement('button');
   complete.classList.add('complete');
   complete.innerHTML = completeSVG;

   //Add click event for completed item
   complete.addEventListener('click',completeItem);


   button.appendChild(remove);
   button.appendChild(complete);
   item.appendChild(button);
   list.insertBefore(item,list.childNodes[0]);
}
 